using UnityEngine;
using System.Collections;

public class CapController : MonoBehaviour {
//Camera cam;
float sense = 5f;
Vector2 rotation = Vector2.zero;

// Use this for initialization
void Start () {
    Debug.Log("Fly script added to: " + gameObject.name);
    //cam = GetComponent<Camera>();
}

// Update is called once per frame
void Update () {
    if (Input.GetKey("w")) {
        transform.position += transform.forward * Time.deltaTime * 10.0f;
    }
    if (Input.GetKey("s")) {
        transform.position += transform.forward * Time.deltaTime * -10.0f;
    }
    if (Input.GetKey(KeyCode.Space)) {
        transform.position += transform.up * Time.deltaTime * 10.0f;
    }
    if (Input.GetKey(KeyCode.LeftShift)) {
        transform.position += transform.up * Time.deltaTime * -10.0f;
    }
    if (Input.GetKey("a")) {
        transform.position += transform.right * Time.deltaTime * -10.0f;
    }
    if (Input.GetKey("d")) {
        transform.position += transform.right * Time.deltaTime * 10.0f;
    }

    
    rotation.y += Input.GetAxis("Mouse X");
    rotation.x += -Input.GetAxis("Mouse Y");
    transform.eulerAngles = (Vector2)rotation * sense;
}
}